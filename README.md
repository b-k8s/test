# Ingress & MetalLB test

* Test MetalLB LoadBalancer by applying `test.yml`
* If you want to try an http (no TLS) ingress test also apply `test_ingress.yml`
* If you want to try a self-signed tls ingress test also apply `test_selfsigned_tls.yml`
* Check deployment objects with `kubectl get all -lapp=hello`
* Delete the test deployment with:
```
kubectl delete pods,services,deployment --selector app=hello
```
